import unittest
import requests

class SimilarityServiceRest:
    host = "http://localhost"
    port = 8080

    def addSpectra(self,spectra,id):
        #adds a new spectra
        requests.post("{}:{}/similarity/add".format(self.host,self.port,id),json = {'upload' : {'spectrum':spectra, 'id':id} })

    def clear(self):
        requests.post("{}:{}/similarity/clear".format(self.host,self.port))

    def commit(self):
        requests.put("{}:{}/similarity/commit".format(self.host,self.port))

    def searchSimilar(self, spectra, minimumScore):
        return requests.post("{}:{}/similarity/search".format(self.host,self.port),json = {'searchRequest' : {'spectrum':spectra, 'minimumScore':minimumScore} }).json()['result']

    def librarySize(self):
      return requests.get("{}:{}/similarity/librarySize".format(self.host,self.port)).json()['result']


class SimilarityServiceRestTest(unittest.TestCase):
    def test(self):
        service = SimilarityServiceRest()

        #clear the cached spectra
        service.clear()

        self.assertEqual(service.librarySize(),0)

        #uploads spectra
        service.addSpectra("100:1 122:2 133:3", "1")

        self.assertEqual(service.librarySize(),1 )

        service.addSpectra("111:1 112:2 123:3 134:4", "2")

        self.assertEqual(service.librarySize(),2 )

        #commits them to the memory for calculations
        service.commit()

        #search for similar spectra against the system
        result = service.searchSimilar("100:1 122:2 133:3",0.9)

        self.assertEqual(len(result),1 )

        print "all tests passed!"



if __name__ == '__main__':
    unittest.main()
