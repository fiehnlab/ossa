//
// Created by Gert Wohlgemuth on 6/27/17.
//

#include "Similarity.h"
#include <iostream>

long Similarity::librarySize() {
    return search->getLibrarySize();
}

void Similarity::commit() {
    search->commitAddedSpectra();
}

void Similarity::add(const UploadRequest upload) {
    //cout << "adding spectra "<< upload.spectrum << " with id " << upload.id << endl;

    search->addLibrarySpectra(upload.id,upload.spectrum);
}

void Similarity::clear() {
    search->clear();
}

std::list<Result> Similarity::similaritySearch(const SearchRequest searchRequest) {

    //cout<< "searching for similar spectra for unknown: "<< searchRequest.spectrum <<endl;
    //cout<< "library size is " << search->getLibrarySize() << " spectra" <<endl;

    std::vector<SimilarityResult> results = search->search(searchRequest.spectrum,searchRequest.minimumScore);

    //cout<< "found "<< results.size() << " results." <<endl;

    std::list<Result> data;

    for(std::vector<SimilarityResult>::iterator it = results.begin(); it != results.end(); ++it) {
        data.push_back(Result{it->score,it->splash});
    }

    return data;
}

Similarity::Similarity() {
    search = new SimilaritySearch((COSINE_SIMILARITY_KERNEL));
}

Similarity::~Similarity() {
    delete this->search;
}
