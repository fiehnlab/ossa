//
// Created by Gert Wohlgemuth on 6/28/17.
//

#ifndef SPECTRUMSIMILARITY_RESULT_H
#define SPECTRUMSIMILARITY_RESULT_H
#include <string>


struct Result{
    float score;

    std::string splash;
};

struct UploadRequest{
    std::string id;

    std::string spectrum;
};

struct SearchRequest{
    std::string spectrum;

    float minimumScore;
};


#endif //SPECTRUMSIMILARITY_RESULT_H
