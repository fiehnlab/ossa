//
// Created by wohlgemuth on 6/19/17.
//

#include <fstream>
#include <iostream>
#include <iomanip>
#include "../include/SimilaritySearch.h"
#include "../../thirdparty/spectra-hash/cpp/src/splash.hpp"
#include "../include/weightedIntensity.h"
#include "../include/compute_cosine.h"
#include "../include/compute_composite.h"

unsigned long SimilaritySearch::getLibrarySize() {
    return this->library.size();
}


void SimilaritySearch::addLibrarySpectra(const string &id, const string &librarySpectrum) {
    //add this spectra to the internal database
    this->library.push_back(Spectrum(id, librarySpectrum));
    this->spectraAdded = true;
}

void SimilaritySearch::addLibrarySpectra(const string &librarySpectrum) {
    addLibrarySpectra(splashIt(librarySpectrum, '1'), librarySpectrum);
}

void SimilaritySearch::loadLibrarySpectra(const string &fileName) {
    ifstream myReadFile;
    myReadFile.open(fileName);
    string line;

    ////cout << "loading file " + fileName + "\n";

    while (getline(myReadFile, line)) {

        //hash defines a comment in the file
        if (line.find("#") != 0) {
            ////cout << line + "\n";

            // Handle input in the following form: [id]\t[spectrum string]
            size_t delim_pos = line.find(',');

            if (delim_pos != std::string::npos) {
                std::string id = line.substr(0, delim_pos);
                std::string spectrum_string = line.substr(delim_pos + 1);
                addLibrarySpectra(id, spectrum_string);

            }
        }
    }

    myReadFile.close();

}

vector<SimilarityResult> SimilaritySearch::search(const string &unknown, float minimumSimilarity) {
    return search(Spectrum(unknown), minimumSimilarity);
}

SimilaritySearch::SimilaritySearch(vector<cl::Device> &devices) {
    this->init(devices, COSINE_SIMILARITY_KERNEL);
}

SimilaritySearch::SimilaritySearch() {
    vector<cl::Platform> platforms;
    vector<cl::Device> devices;

    util.findDevice(platforms, devices, CL_DEVICE_TYPE_GPU);

    this->init(devices, COSINE_SIMILARITY_KERNEL);
}

SimilaritySearch::SimilaritySearch(string kernelFile) {
    vector<cl::Platform> platforms;
    vector<cl::Device> devices;

    util.findDevice(platforms, devices, CL_DEVICE_TYPE_GPU);

    this->init(devices, kernelFile);
}

SimilaritySearch::SimilaritySearch(vector<cl::Device> &devices, string kernelFile) {
    this->init(devices, kernelFile);
}

/**
 * TODO figure out how to run on multiple devices.
 *
 * @param devices
 * @param kernelFile
 */
void SimilaritySearch::init(vector<cl::Device> &devices, string kernelFile) {

    this->library.reserve(1000000);

    cl::Program::Sources sources;

    //build our sources for the program to be executed
//    //cout << "added to sources"<< endl;

    sources.push_back(make_pair(weightedIntensity_ocl, strlen(weightedIntensity_ocl)));

    if(kernelFile == COSINE_SIMILARITY_KERNEL){
        sources.push_back(make_pair(compute_cosine_ocl, strlen(compute_cosine_ocl)));
    }
    else if(kernelFile == COMPOSITE_SIMILARITY_KERNEL){
        sources.push_back(make_pair(compute_composite_ocl, strlen(compute_composite_ocl)));
    }
    else{
        cerr << "invalid kernel specified" << endl;
        exit(-1);
    }

//    //cout << "defining context" << endl;

    context = make_shared<cl::Context>(cl::Context(devices));

//    //cout << "building program" << endl;

    //compile our sources
    program = make_shared<cl::Program>(*context.get(), sources);

    if (program->build({ devices }) != CL_SUCCESS)
    {
        std::cout<<" Error building: "<<program->getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices.front())<<"\n";
        exit(-1);
    }

//    //cout << "creating queue"<<endl;

    /*
    //define 1 queue for each known device
    for(auto const& device:devices) {

        queues.push_back(cl::CommandQueue(*context.get(), device));
    }
     */

    queue = make_shared<cl::CommandQueue>(cl::CommandQueue(*context.get(), devices.front()));

    this->similarityKernel = make_shared<cl::Kernel>(cl::Kernel(*program, kernelFile.c_str()));

    this->weightedIntensityKernel = make_shared<cl::Kernel>( cl::Kernel(*program, "weightedIntensity"));

//    //cout << "finished init";
}

/**
 * TODO figure out how to utilize all available devices. One apprach would be to split the given data matrix into one matrix for each device. Right now we only work on the first found device
 */
void SimilaritySearch::commitAddedSpectra() {
    spectraAdded = false;

//    //cout << "commiting spectra" << endl;

    int numCols = MAX_ION_COUNT_FOR_SPECTRA;
    int numRows = (int) this->getLibrarySize();

    int *spectra = new int[numRows * numCols];
    int *weightedIntensity = new int[numCols * numRows];

    memset(spectra, 0, sizeof(int) * numRows * numCols);
    memset(weightedIntensity, 0, sizeof(int) * numRows * numCols);

    util.flattenToArray(this->library, spectra, numCols);

    //free up potentially used old buffers
    if(buffer != NULL){

        vector<cl::Memory> toRelease;
        toRelease.push_back(*buffer);

        queue->enqueueReleaseGLObjects(&toRelease,NULL,NULL);
        buffer=NULL;
    }

//    //cout << "creating buffers" << endl;

    std::shared_ptr<cl::Buffer> buf_arr = std::make_shared<cl::Buffer>(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(int) * numRows * numCols, spectra);

    buffer= std::make_shared<cl::Buffer>(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                     sizeof(int) * numRows * numCols, spectra);

    this->weightedIntensityKernel->setArg(0, buf_arr);
    this->weightedIntensityKernel->setArg(1, buffer);


//    //cout << "starting kernels" << endl;


    queue -> enqueueNDRangeKernel(*weightedIntensityKernel.get(), cl::NullRange, cl::NDRange(numRows, numCols));
    queue -> finish();

    vector<cl::Memory> toRelease;
    toRelease.push_back(*buf_arr);

    queue  -> enqueueReleaseGLObjects(&toRelease,NULL,NULL);
//    //cout << "cleaning up" << endl;

    //memory error happens somehow here. somhehow use a shared pointer instead
    delete[] spectra;
    delete[] weightedIntensity;
//    //cout << "commited spectra to memory" << endl;

    toRelease.clear();
    buf_arr=NULL;
}

/**
 * todo make this work on multiple devices
 * @param unknown
 * @param minimumSimilarity
 * @return
 */
vector<SimilarityResult> SimilaritySearch::search(const Spectrum &unknown, float minimumSimilarity) {
    if(spectraAdded){
        //cout << "you forgot to commit the spectra to the GPU memory, doing it for you!" << endl;
        commitAddedSpectra();
    }

    //cout << "searching for possible matches for " << unknown.getOriginal() << " with a minimum similarity off " << minimumSimilarity << endl;

    int numCols = MAX_ION_COUNT_FOR_SPECTRA;
    int numRows = (int) this->getLibrarySize();

    //unknown flattened spectrum
    int *unknownSpectrum = new int[numCols];
    memset(unknownSpectrum, 0, sizeof(int) * numCols);

    vector<Spectrum> tmp;
    tmp.push_back(unknown);
    util.flattenToArray(tmp, unknownSpectrum, numCols);

    //contains our results
    float *similarityScore = new float[numRows];

    //  create a buffer object in device memory call
    std::shared_ptr<cl::Buffer> buf_similarityScore = std::make_shared<cl::Buffer>(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                   sizeof(float) * numRows, similarityScore);
    std::shared_ptr<cl::Buffer> buf_unknown = std::make_shared<cl::Buffer>(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                               sizeof(int) * numCols, unknownSpectrum);
    std::shared_ptr<cl::Buffer> buf_numCols = std::make_shared<cl::Buffer>(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int),
                           &numCols);
    //set our kernel arguments
    this->similarityKernel->setArg(0, buffer);
    this->similarityKernel->setArg(1, buf_similarityScore);
    this->similarityKernel->setArg(2, buf_unknown);
    this->similarityKernel->setArg(3, buf_numCols);

    queue->enqueueNDRangeKernel(*this->similarityKernel, cl::NullRange, cl::NDRange(numRows));
    queue->enqueueReadBuffer(*buf_similarityScore, CL_TRUE, 0, sizeof(float) * numRows, similarityScore);


    vector<SimilarityResult> result;

    //cout << "evaluated similary scores for " << getLibrarySize() << " spectra"<< endl;

    //filter results by score
    int cnt = 0;
    for (std::vector<Spectrum>::iterator it = library.begin(); it != library.end(); ++it) {
        //cout << "score is "<< similarityScore[cnt]<<" for " << it->getSplash() << " of spectra " << it->getOriginal()<<endl;
        if (similarityScore[cnt] > minimumSimilarity) {

            //add to result vector
            result.push_back(SimilarityResult {similarityScore[cnt],it->getSplash() } );
        }
        cnt++;
    }

    //cout << "found "<< result.size() << " possible hits with a minimum similarity over " << minimumSimilarity << endl;
    //cleanup

    vector<cl::Memory> toRelease;
    toRelease.push_back(*buf_similarityScore);
    toRelease.push_back(*buf_unknown);
    toRelease.push_back(*buf_numCols);

    queue->enqueueReleaseGLObjects(&toRelease,NULL,NULL);
    toRelease.clear();
    delete[] similarityScore;
    delete[] unknownSpectrum;

    //return the results
    return result;
}

SimilaritySearch::~SimilaritySearch() {

    library.clear();

    similarityKernel = NULL;
    weightedIntensityKernel = NULL;

    context = NULL;
}

void SimilaritySearch::clear() {
    if(library.size() > 0) {
        library.clear();
    }

    if(buffer != NULL){

        vector<cl::Memory> toRelease;
        toRelease.push_back(*buffer);

        queue->enqueueReleaseGLObjects(&toRelease,NULL,NULL);
        buffer=NULL;
    }
}

