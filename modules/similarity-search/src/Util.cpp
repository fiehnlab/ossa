//
// Created by Gert Wohlgemuth on 6/20/17.
//

#include <array>
#include <cstdlib>
#include <vector>
#include <sstream>
#include <iostream>
#include "../include/Util.h"
#include "../include/SimilaritySearch.h"


double Util::calculateWeightedIntensity(double mz, double intensity) {
    return (int)intensity;
};

int Util::roundMZ(double mz) {
    return (int)(mz + 0.2);
};

array<int, MAX_ION_COUNT_FOR_SPECTRA> Util::convertSpectra(const string &spectrum_string) {
    array<int, MAX_ION_COUNT_FOR_SPECTRA> spectrum;
    spectrum.fill(0);

    // Convert spectrum to a vector of ion pairs
    vector<string> ion_strings = split(spectrum_string, ' ');

    for (vector<string>::iterator it = ion_strings.begin(); it != ion_strings.end(); ++it) {
        size_t delim_pos = (*it).find(':');

        double mz = atof((*it).substr(0, delim_pos).c_str());
        double intensity = atof((*it).substr(delim_pos + 1).c_str());

        // Store ion as a pair object, with 'first' corresponding to m/z and 'second' to intensity
        int binnedMZ = roundMZ(mz);
        spectrum[binnedMZ] += (int)calculateWeightedIntensity(mz, intensity);
    }

    return spectrum;
}

vector<string> Util::split(const string &s, char delimeter) {
    vector<string> elements;
    stringstream ss(s);
    string element;

    while (getline(ss, element, delimeter)) {
        elements.push_back(element);
    }
    return elements;
}

void Util::flattenToArray(vector<array<int, MAX_ION_COUNT_FOR_SPECTRA>> &spectra, int *spec, int numCols) {

    int id_spec = 0;
    for (array<int, MAX_ION_COUNT_FOR_SPECTRA> spectrum : spectra) {
        for(int i = 0; i < MAX_ION_COUNT_FOR_SPECTRA; i++) {
            spec[id_spec * numCols + i] = spectrum[i];
        }
        id_spec++;
    }
}

void Util::findDevice(std::vector<cl::Platform> &platforms, std::vector<cl::Device> &devices, cl_device_type type) {
    cl::Platform::get(&platforms);
    if (platforms.size() == 0) {
        //cout << " No platforms found. Check OpenCL installation!\n";
        exit(-2);
    } else {
        //cout << " found " << platforms.size() << " platforms";

    }
    cl::Platform platform = platforms.front();

    std::cout << "Using platform: " << platform.getInfo<CL_PLATFORM_NAME>() << "\n";

    platform.getDevices(type, &devices);

    if (devices.size() == 0) {
        std::cout << " No devices found. Check OpenCL installation!\n";
        exit(-3);
    } else {
        std::cout << devices.size() << " devices found.\n";
    }
}

void Util::flattenToArray(vector<Spectrum> &spectra, int *spec, int numCols) {
    int id_spec = 0;
    for (Spectrum spectrum : spectra) {
        array<int, MAX_ION_COUNT_FOR_SPECTRA> s = spectrum.getSpectra();

        for(int i = 0; i < MAX_ION_COUNT_FOR_SPECTRA; i++) {
            spec[id_spec * numCols + i] = s[i];
        }
        id_spec++;
    }

}

std::string Util::randomSpectra() {
    std::stringstream spec;
    for (int x = 1; x < MAX_ION_COUNT_FOR_SPECTRA; x++) {
        spec << x << ':' << rand() % 100 + 1;

        if (x < MAX_ION_COUNT_FOR_SPECTRA - 1) {
            spec << ' ';
        }
    }
    return spec.str();
}
