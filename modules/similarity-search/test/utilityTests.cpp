//
// Created by Gert Wohlgemuth on 6/20/17.
//


#include "gtest/gtest.h"
#include "../include/Util.h"
#include "../include/SimilaritySearch.h"

TEST(Utilites, splitTest) {

    Util util;

    //our test
    vector<string> testResult = util.split("12:1 13:2 14:3 15:4", ' ');

    //evaluate our results
    EXPECT_EQ(testResult[0],"12:1");
    EXPECT_EQ(testResult[1],"13:2");
    EXPECT_EQ(testResult[2],"14:3");
    EXPECT_EQ(testResult[3],"15:4");

}

TEST(Utilities, convertSpectraTest) {
    Util util;

    array<int, MAX_ION_COUNT_FOR_SPECTRA> testResult = util.convertSpectra("12:1 13:2 14:3 15:4");

    EXPECT_EQ(testResult[12],1);
    EXPECT_EQ(testResult[13],2);
    EXPECT_EQ(testResult[14],3);
    EXPECT_EQ(testResult[15],4);

}

TEST(Utilities, flattenToArray) {

    Util util;


    vector<array<int, MAX_ION_COUNT_FOR_SPECTRA>> data;

    data.push_back(util.convertSpectra("12:1 13:2 14:3 15:4"));
    data.push_back(util.convertSpectra("12:5 13:10 14:15 15:20"));

    int *spectra = new int[data.size()*MAX_ION_COUNT_FOR_SPECTRA];

    util.flattenToArray(data,spectra,MAX_ION_COUNT_FOR_SPECTRA);


    EXPECT_EQ(spectra[12],1);
    EXPECT_EQ(spectra[13],2);

    EXPECT_EQ(spectra[MAX_ION_COUNT_FOR_SPECTRA+12],5);
    EXPECT_EQ(spectra[MAX_ION_COUNT_FOR_SPECTRA+13],10);


    delete [] spectra;

}

TEST(Utilities, flattenToArray2) {

    Util util;


    vector<Spectrum> data;

    data.push_back(Spectrum("12:1 13:2 14:3 15:4"));
    data.push_back(Spectrum("12:5 13:10 14:15 15:20"));

    int *spectra = new int[data.size()*MAX_ION_COUNT_FOR_SPECTRA];

    util.flattenToArray(data,spectra,MAX_ION_COUNT_FOR_SPECTRA);


    EXPECT_EQ(spectra[12],1);
    EXPECT_EQ(spectra[13],2);

    EXPECT_EQ(spectra[MAX_ION_COUNT_FOR_SPECTRA+12],5);
    EXPECT_EQ(spectra[MAX_ION_COUNT_FOR_SPECTRA+13],10);


    delete [] spectra;

}