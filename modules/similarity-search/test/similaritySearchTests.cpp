//
// Created by Gert Wohlgemuth on 6/20/17.
//

#include "gtest/gtest.h"
#include "../include/SimilarityResult.h"
#include "../include/SimilaritySearch.h"
/*
 * Author:  David Robert Nadeau
 * Site:    http://NadeauSoftware.com/
 * License: Creative Commons Attribution 3.0 Unported License
 *          http://creativecommons.org/licenses/by/3.0/deed.en_US
 */

#if defined(_WIN32)
#include <windows.h>
#include <psapi.h>

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
#include <unistd.h>
#include <sys/resource.h>

#if defined(__APPLE__) && defined(__MACH__)
#include <mach/mach.h>

#elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
#include <fcntl.h>
#include <procfs.h>

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
#include <stdio.h>

#endif

#else
#error "Cannot define getPeakRSS( ) or getCurrentRSS( ) for an unknown OS."
#endif





/**
 * Returns the peak (maximum so far) resident set size (physical
 * memory use) measured in bytes, or zero if the value cannot be
 * determined on this OS.
 */
size_t getPeakRSS( )
{
#if defined(_WIN32)
    /* Windows -------------------------------------------------- */
    PROCESS_MEMORY_COUNTERS info;
    GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
    return (size_t)info.PeakWorkingSetSize;

#elif (defined(_AIX) || defined(__TOS__AIX__)) || (defined(__sun__) || defined(__sun) || defined(sun) && (defined(__SVR4) || defined(__svr4__)))
    /* AIX and Solaris ------------------------------------------ */
    struct psinfo psinfo;
    int fd = -1;
    if ( (fd = open( "/proc/self/psinfo", O_RDONLY )) == -1 )
        return (size_t)0L;      /* Can't open? */
    if ( read( fd, &psinfo, sizeof(psinfo) ) != sizeof(psinfo) )
    {
        close( fd );
        return (size_t)0L;      /* Can't read? */
    }
    close( fd );
    return (size_t)(psinfo.pr_rssize * 1024L);

#elif defined(__unix__) || defined(__unix) || defined(unix) || (defined(__APPLE__) && defined(__MACH__))
    /* BSD, Linux, and OSX -------------------------------------- */
    struct rusage rusage;
    getrusage( RUSAGE_SELF, &rusage );
#if defined(__APPLE__) && defined(__MACH__)
    return (size_t)rusage.ru_maxrss;
#else
    return (size_t)(rusage.ru_maxrss * 1024L);
#endif

#else
    /* Unknown OS ----------------------------------------------- */
    return (size_t)0L;          /* Unsupported. */
#endif
}





/**
 * Returns the current resident set size (physical memory use) measured
 * in bytes, or zero if the value cannot be determined on this OS.
 */
size_t getCurrentRSS( )
{
#if defined(_WIN32)
    /* Windows -------------------------------------------------- */
    PROCESS_MEMORY_COUNTERS info;
    GetProcessMemoryInfo( GetCurrentProcess( ), &info, sizeof(info) );
    return (size_t)info.WorkingSetSize;

#elif defined(__APPLE__) && defined(__MACH__)
    /* OSX ------------------------------------------------------ */
    struct mach_task_basic_info info;
    mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;
    if ( task_info( mach_task_self( ), MACH_TASK_BASIC_INFO,
        (task_info_t)&info, &infoCount ) != KERN_SUCCESS )
        return (size_t)0L;      /* Can't access? */
    return (size_t)info.resident_size;

#elif defined(__linux__) || defined(__linux) || defined(linux) || defined(__gnu_linux__)
    /* Linux ---------------------------------------------------- */
    long rss = 0L;
    FILE* fp = NULL;
    if ( (fp = fopen( "/proc/self/statm", "r" )) == NULL )
        return (size_t)0L;      /* Can't open? */
    if ( fscanf( fp, "%*s%ld", &rss ) != 1 )
    {
        fclose( fp );
        return (size_t)0L;      /* Can't read? */
    }
    fclose( fp );
    return (size_t)rss * (size_t)sysconf( _SC_PAGESIZE);

#else
    /* AIX, BSD, Solaris, and Unknown OS ------------------------ */
    return (size_t)0L;          /* Unsupported. */
#endif
}

TEST(SimilarityResult,evaluateResult) {
    SimilarityResult result{0.5,"test"};

    EXPECT_EQ(result.score,0.5);
    EXPECT_EQ(result.splash,"test");
}

TEST(SimilaritySearch,addLibrarySpectra) {
    SimilaritySearch search;

    EXPECT_EQ(search.getLibrarySize(),0);
    int count = 10;
    for(int i = 0; i < count; i++) {
        search.addLibrarySpectra("10:1 11:2 20:3 33:3 44:" + to_string(i));
        EXPECT_EQ(search.getLibrarySize(),i+1);
    }
    EXPECT_EQ(search.getLibrarySize(),count);

    //search.commitAddedSpectra();
}

TEST(SimilaritySearch,loadLibrarySpectra){
    SimilaritySearch search;

    EXPECT_EQ(search.getLibrarySize(),0);
    search.loadLibrarySpectra("testSpectra.txt");
    EXPECT_EQ(search.getLibrarySize(),6);

    search.commitAddedSpectra();

}


TEST(SimilaritySearch,searchCosine) {
    SimilaritySearch search;

    EXPECT_EQ(search.getLibrarySize(),0);
    int count = 10;
    for(int i = 0; i < count; i++) {
        search.addLibrarySpectra("10:1 11:2 20:3 33:3 44:" + to_string(i));
    }

    search.commitAddedSpectra();
    clock_t start;


    start = clock();
    vector<SimilarityResult> result = search.search("10:1 11:2 20:3 33:3 44:0",0.5);

    //cout << count << " spectra cosine search speed: " << (clock() - start) * 1000 / CLOCKS_PER_SEC << " ms " << endl;
}


TEST(SimilaritySearch,searchComposite) {
    SimilaritySearch search(COMPOSITE_SIMILARITY_KERNEL);

    EXPECT_EQ(search.getLibrarySize(),0);
    int count = 10;
    for(int i = 0; i < count; i++) {
        search.addLibrarySpectra("10:1 11:2 20:3 33:3 44:" + to_string(i));
    }

    search.commitAddedSpectra();
    clock_t start;


    start = clock();
    vector<SimilarityResult> result = search.search("10:1 11:2 20:3 33:3 44:0",0.5);

    //cout << count << " spectra composite search speed: " << (clock() - start) * 1000 / CLOCKS_PER_SEC << " ms " << endl;

}


TEST(SimilaritySearch,searchCompositeLong) {
    SimilaritySearch search(COMPOSITE_SIMILARITY_KERNEL);
    Util util;

    EXPECT_EQ(search.getLibrarySize(), 0);
    int count = 1;
    int spectraIncrement = 10;
    int commitSamples = 5;
    int searchSamples = 10;

    clock_t start;

    cout<< "operation duration librarySize samples memory(mb)" << endl;

    for(int y = 0; y < count; y++ ) {

        for (int i = 0; i < spectraIncrement; i++) {
            //cout << spec << endl;
            search.addLibrarySpectra(util.randomSpectra());
        }


        double duration = 0;

        for (int i = 0; i < commitSamples; i++) {

            start = clock();

            search.commitAddedSpectra();
            duration = duration + (clock() - start) * 1000 / CLOCKS_PER_SEC;

        }

        duration = duration/commitSamples;

        cout<< "commit " << duration << " " << search.getLibrarySize() << " " << commitSamples << " " << getCurrentRSS() / 1024 /1024 <<endl;

        duration = 0;
        for (int i = 0; i < searchSamples; i++) {
            string spec = util.randomSpectra();
            start = clock();
            vector<SimilarityResult> result = search.search(spec, 0.5);

            duration = duration + (clock() - start) * 1000 / CLOCKS_PER_SEC;

        }

        duration = duration /searchSamples;

        cout<< "search " << duration << " " << search.getLibrarySize() << " " << searchSamples << " " << getCurrentRSS() / 1024 /1024  <<endl;

    }

    cout<< "total " << (clock() - start) * 1000 / CLOCKS_PER_SEC << " " << search.getLibrarySize() << " " << searchSamples << " " << getCurrentRSS() / 1024 /1024 <<endl;
    search.clear();
    cout<< "cleared " << (clock() - start) * 1000 / CLOCKS_PER_SEC << " " << search.getLibrarySize() << " " << searchSamples << " " << getCurrentRSS() / 1024 /1024 <<endl;

}

TEST(SimilaritySearch,accurateMassSearchComposite) {
    SimilaritySearch search(COMPOSITE_SIMILARITY_KERNEL);

    EXPECT_EQ(search.getLibrarySize(),0);
    int count = 10;
    for(int i = 0; i < count; i++) {
        search.addLibrarySpectra("10:1 11:2 20:3 33:3 44:" + to_string(i));
    }

    search.commitAddedSpectra();
    clock_t start;


    start = clock();
    vector<SimilarityResult> result = search.search("10:1 11:2 20:3 33.1:1 33.2:2 44:0",0.5);

    cout << count << " spectra composite search speed: " << (clock() - start) * 1000 / CLOCKS_PER_SEC << " ms " << endl;
}

TEST(SimilaritySearch,cosineSimilarityValidity){
    SimilaritySearch search(COSINE_SIMILARITY_KERNEL);

    EXPECT_EQ(search.getLibrarySize(), 0);

    search.addLibrarySpectra("10:1");
    search.commitAddedSpectra();

    EXPECT_EQ(search.getLibrarySize(), 1);

    vector<SimilarityResult> result = search.search("10:1", 0.0);

    cout << "cosineSimilarityValidity" << endl;
    cout << result.size() << endl;

    EXPECT_EQ(result.size(), 1);
}