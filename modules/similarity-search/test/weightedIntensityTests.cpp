// TODO : correct include path when move back to OSSA repo
#define __CL_ENABLE_EXCEPTIONS
#include "gtest/gtest.h"
#include "cl.hpp"
#include <utility>
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>
#include <unistd.h>
#define FILE_IN     "NIST_SPECTRA_31.csv"
#define FILE_IN_WEIGHTED_INTENSITY_EXPECTED     "NIST_SPECTRA_31.csv"
#define FILE_IN_COSINE_EXPECTED     "NIST_SPECTRA_31_COSINE.csv"
#define FILE_IN_COMPOSITE_EXPECTED  "NIST_SPECTRA_31_COMPOSITE.csv"

namespace std
{
    struct kernelTests : public ::testing::Test
    {
        std::vector<cl::Platform> platforms;
        cl::Platform platform;
        std::vector<cl::Device> devices;
        cl::Program::Sources sources;
        cl::Device device;
        cl::Context context;
        cl_device_type type = CL_DEVICE_TYPE_CPU;
        cl::Program program;
        cl::CommandQueue queue;
        cl::Kernel kernelWeightedIntensity, kernel;
        cl::Buffer bufferArray, bufferWeightedIntensity;
        cl::Buffer bufferSimilarityScore, bufferRowUnknown, bufferNumCols;
        
        int numCols = 1000, numRows = 35, rowUnknown = 1, numSpectrum = 0;
        
        float absErr = 1e-5;
        
        string* specId = new string[ numRows ];
        int* weightedIntensity = new int[ numRows * numCols ];
        int* spectra = new int[ numRows * numCols ];
        int* weightedIntensityExpected = new int[ numCols * numRows ];
        float* similarityScore = new float[ numRows ];
        float* similarityScoreExpected = new float[ numRows * numRows ];
        
        kernelTests(){
            getPlatforms();
            getDevices();
            loadKernels();
            buildKernels();
            init();
        }
        
        ~kernelTests(){
            delete[] specId;
            delete[] weightedIntensity;
            delete[] spectra;
            delete[] weightedIntensityExpected;
            delete[] similarityScore;
            delete[] similarityScoreExpected;
        }
        
        void getPlatforms(){
            
            cl::Platform::get( &platforms );
            if( platforms.size() == 0 ){
                std::cout << " No platforms found. Check OpenCL installation!\n";
                exit(-1);
            } else {
                std::cout << " found " << platforms.size() << " platforms";
            }
            
            platform = cl::Platform( platforms.front() );
            std::cout << "Using platform: " << platform.getInfo< CL_PLATFORM_NAME >() << "\n";
        }
        
        void getDevices(){
            
            platform.getDevices( type, &devices );
            if( devices.size() == 0 ){
                std::cout << " No devices found. Check OpenCL installation!\n";
                exit(-2);
            } else {
                std::cout << devices.size() << " devices found.\n";
            }
            
            // print first device information
            device = devices.front();
            std::cout << "Using device: " << device.getInfo< CL_DEVICE_NAME >() << "\n";
            std::cout << "Vendor: " << device.getInfo< CL_DEVICE_VENDOR >() << "\n";
            std::cout << "Version: " << device.getInfo< CL_DEVICE_VERSION >() << "\n";
            std::cout << "CL_DEVICE_MAX_MEM_ALLOC_SIZE: " << device.getInfo< CL_DEVICE_MAX_MEM_ALLOC_SIZE >() / 1024 / 1024 << "MB\n";
        }
        
        void loadKernels(){
            
            //load kernel source files
            std::ifstream oclWeightedIntensity( "weightedIntensity.cl" );
            std::string src1( std::istreambuf_iterator<char>( oclWeightedIntensity ), ( std::istreambuf_iterator<char>() ));
            sources.push_back( std::make_pair( src1.c_str(), src1.length() ));
            
            std::ifstream oclComputeCosine( "compute_cosine.cl" );
            std::string src2(std::istreambuf_iterator<char>( oclComputeCosine ), ( std::istreambuf_iterator<char>() ));
            sources.push_back( std::make_pair( src2.c_str(), src2.length() ));
            
            std::ifstream oclComputeComposite( "compute_composite.cl" );
            std::string src3(std::istreambuf_iterator<char>( oclComputeComposite ), ( std::istreambuf_iterator<char>() ));
            sources.push_back( std::make_pair( src3.c_str(), src3.length() ));
        }
        
        void buildKernels(){
            
            //create OpenCL context
            context = cl::Context( device );
            
            //create OpenCl program from soure files
            program = cl::Program( context, sources );
            if(program.build( devices ) != CL_SUCCESS){
                std::cout << " Error building: " << program.getBuildInfo< CL_PROGRAM_BUILD_LOG >( device )<<"\n";
                exit(-3);
            }
            
            //create command queue
            queue = cl::CommandQueue( context, device );
        }
        
        void init(){
            
            memset( similarityScore, 0, sizeof(float) * numRows );
            memset( similarityScoreExpected, 0, sizeof(float) * numRows * numRows );
        }
        
        void loadTestData(std::string filename, int* spectra){
            
            std::ifstream myReadFile;
            myReadFile.open(filename);
            std::string line;
            numSpectrum = 0;
            
            if (myReadFile.is_open()) {
                while(getline(myReadFile, line)){
                    
                    // Handle input in the following form: [id],[spectrum string]
                    size_t delim_pos = line.find(',');
                    if (delim_pos != std::string::npos) {
                        specId[numSpectrum] = line.substr(0, delim_pos);
                        std::string spectrum_string = line.substr(delim_pos + 1);
                        std::vector<std::string> ion_strings = split(spectrum_string, ' ');

                        for (vector<string>::iterator it = ion_strings.begin(); it != ion_strings.end(); ++it) {
                            size_t delim_pos = (*it).find(':');
                            
                            int mz = atoi((*it).substr(0, delim_pos).c_str());
                            int intensity = atof((*it).substr(delim_pos + 1).c_str());
                            
                            spectra[numSpectrum * numCols + mz] = intensity;
                        }
                    }
                    numSpectrum++;
                }
            }
            else{
                std::cout << filename << " :file not found" << std::endl;
            }
            myReadFile.close();
        }
        
        void loadExpectedReturn(std::string filename, float* scores){
            
            std::ifstream myReadFile;
            myReadFile.open(filename);
            std::string line;
            int currentRow = 0;
            if (myReadFile.is_open()) {
                while(getline(myReadFile, line)){
                    int currentCol = 0;
                    std::vector<std::string> score_strings = split(line, ',');
                    for (vector<string>::iterator it = score_strings.begin(); it != score_strings.end(); ++it) {
                        float score = atof((*it).c_str());
                        scores[currentRow * numRows + currentCol] = score;
                        currentCol++;
                    }
                    currentRow++;
                }
            }
            else{
                std::cout << filename << ": file not found" << std::endl;
            }
            myReadFile.close();
        }
        
        void loadBuffers(float *similarityScore){
            
            bufferWeightedIntensity = cl::Buffer( context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int) * numSpectrum * numCols, weightedIntensityExpected );
            bufferSimilarityScore = cl::Buffer( context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float) * numSpectrum, similarityScore );
            bufferRowUnknown = cl::Buffer( context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), &rowUnknown );
            bufferNumCols = cl::Buffer( context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), &numCols );
        }
        
        // TODO: duplicate with Util::split
        vector<string> split(const string &s, char delimeter) {
            vector<string> elements;
            stringstream ss(s);
            string element;
            
            while (getline(ss, element, delimeter)) {
                elements.push_back(element);
            }
            return elements;
        }
        
        void setKernelArgs(const char * kernelFile){
            
            //sets arguments for both cosine and composite method kernel
            kernel = cl::Kernel( program, kernelFile );
            kernel.setArg(0, bufferWeightedIntensity);
            kernel.setArg(1, bufferSimilarityScore);
            kernel.setArg(3, bufferNumCols);
        }
        
        void computePairwiseScores(int rowUnknown){
            
            //pick one exsiting spectrum as unknown to perform an pairwise similarity score computation
            bufferRowUnknown = cl::Buffer( context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int), &rowUnknown );
            kernel.setArg(2, bufferRowUnknown);
            
            
            //enqueue the kernel
            queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(numSpectrum));
            
            //transfer result back to host
            queue.enqueueReadBuffer(bufferSimilarityScore, CL_TRUE, 0, sizeof(float) * numSpectrum, similarityScore );
            
        }
    };

    TEST_F(kernelTests, weightedIntensityTest) {
        
        //load test data and corresponding expected output
        loadTestData(FILE_IN, spectra);
        loadTestData(FILE_IN_WEIGHTED_INTENSITY_EXPECTED, weightedIntensityExpected);
        
        //create OpenCL kernel
        kernelWeightedIntensity = cl::Kernel( program, "weightedIntensity" );
        
        //create buffer on device and copy data over
        bufferArray = cl::Buffer( context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int) * numSpectrum * numCols, spectra );
        bufferWeightedIntensity = cl::Buffer( context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int) * numSpectrum * numCols, weightedIntensity );
        
        //set kernel arguments
        kernelWeightedIntensity.setArg( 0, bufferArray );
        kernelWeightedIntensity.setArg( 1, bufferWeightedIntensity );
        
        //compute weighted intensity and read back into an array
        queue.enqueueNDRangeKernel( kernelWeightedIntensity, cl::NullRange, cl::NDRange( numSpectrum, numCols ));
        queue.enqueueReadBuffer( bufferWeightedIntensity, CL_TRUE, 0, sizeof(int) * numSpectrum * numCols, weightedIntensity);
        
        EXPECT_EQ( memcmp(weightedIntensity, weightedIntensityExpected, numSpectrum * numCols * sizeof(int)), 0);
    }
 
    
    TEST_F(kernelTests, computeCosineTest) {
        
        //load test data and corresponding expected output
        loadTestData(FILE_IN_WEIGHTED_INTENSITY_EXPECTED, weightedIntensityExpected);
        loadExpectedReturn(FILE_IN_COSINE_EXPECTED, similarityScoreExpected);
        loadBuffers(similarityScore);
        
        
        //load kernel and set kernel arguments
        const char * kernelFile = "compute_cosine";
        setKernelArgs(kernelFile);
        
        
        //iterate through each speactra pair to test the similarity score
        for(int rowUnknown = 0; rowUnknown < numSpectrum; rowUnknown++){
            computePairwiseScores(rowUnknown);
            for(int i = 0; i < numSpectrum; i++){
                EXPECT_NEAR(similarityScoreExpected[rowUnknown * numRows + i], similarityScore[i], absErr);
            }
        }
    }
    
    TEST_F(kernelTests, computeCompositeTest) {
        
        //load test data and corresponding expected output
        loadTestData(FILE_IN_WEIGHTED_INTENSITY_EXPECTED, weightedIntensityExpected);
        loadExpectedReturn(FILE_IN_COMPOSITE_EXPECTED, similarityScoreExpected);
        loadBuffers(similarityScore);
        
        
        //load kernel and set kernel arguments
        const char * kernelFile = "compute_composite";
        setKernelArgs(kernelFile);
        
        
        //iterate through each speactra pair to test the similarity score
        for(int rowUnknown = 0; rowUnknown < numSpectrum; rowUnknown++){
            computePairwiseScores(rowUnknown);
            for(int i = 0; i < numSpectrum; i++){
                EXPECT_NEAR(similarityScoreExpected[rowUnknown * numRows + i], similarityScore[i], absErr);
            }
        }
    }
};
