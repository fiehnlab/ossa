//
// Created by Gert Wohlgemuth on 6/20/17.
//

#include "gtest/gtest.h"
#include "../include/Spectrum.h"

TEST(Spectrum, constructorTest) {

    Spectrum test("1","12:10 13:12 34:12");
    array<int, MAX_ION_COUNT_FOR_SPECTRA> reference = test.getSpectra();

    EXPECT_EQ(reference[12],10);
    EXPECT_EQ(reference[13],12);
    EXPECT_EQ(reference[34],12);

    EXPECT_EQ(test.getSplash(),"splash10-03e9-9000000000-f2addd9d59fc784819dc");
    EXPECT_EQ(test.getId(),"1");
}


TEST(Spectrum, constructorTest2) {

    Spectrum test("12:10 13:12 34:12");
    array<int, MAX_ION_COUNT_FOR_SPECTRA> reference = test.getSpectra();

    EXPECT_EQ(reference[12],10);
    EXPECT_EQ(reference[13],12);
    EXPECT_EQ(reference[34],12);

    EXPECT_EQ(test.getSplash(),"splash10-03e9-9000000000-f2addd9d59fc784819dc");
    EXPECT_EQ(test.getId(),"splash10-03e9-9000000000-f2addd9d59fc784819dc");

}