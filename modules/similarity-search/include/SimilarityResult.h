//
// Created by wohlgemuth on 6/19/17.
//

#ifndef PROJECT_SIMILARITYRESULT_H
#define PROJECT_SIMILARITYRESULT_H
#include <string>

/**
 * provides the calculation result
 */
struct SimilarityResult {

    float score;

    std::string splash;
};


#endif //PROJECT_SIMILARITYRESULT_H
