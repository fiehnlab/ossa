//
// Created by Gert Wohlgemuth on 6/20/17.
//

#ifndef SPECTRUMSIMILARITY_SPECTRUM_H
#define SPECTRUMSIMILARITY_SPECTRUM_H


#include <string>
#include <array>
#include "SimilarityConfig.h"

using namespace std;

/**
 * defines a spectrum in the system
 */
class Spectrum {

public:

    /**
     * constructor to actually define a spectra
     * @param spectra
     */
    Spectrum(const string &id, const string &spectra);

    /**
     * defines a new spectrum
     * @param spectra
     */
    Spectrum(const string &spectra);


    /**
         * returns the unique splash indentifier
         * @return
         */
    string getSplash();

    /**
     * returns the id of the spectra
     * @return
     */
    string getId();
    /**
     * returns the internal spectra representation
     */
    array<int, MAX_ION_COUNT_FOR_SPECTRA> getSpectra();

private:

    /**
     * the computed splash string
     */
    string splash;

    /**
     * the internal representation of the spectrum
     * mapped as integers for now
     *
     * obviously this does not support accurate mass spectra at this stage
     */
    array<int, MAX_ION_COUNT_FOR_SPECTRA> spectra;

    /**
     * internal id of the spectra
     */
    string id;
};


#endif //SPECTRUMSIMILARITY_SPECTRUM_H
