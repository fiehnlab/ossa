
kernel void weightedIntensity(global int* input, global int* output){
    
    const int n = 1; // Power to the Peak Intensity;
    const int m = 0; // Power to the Mass;
    int id0 = get_global_id(0);//first dimension : col index
    int id1 = get_global_id(1);//second dimension: row index
    
    int id = (id0 * get_global_size(1)) + id1;
    output[id] = input[id];
}
